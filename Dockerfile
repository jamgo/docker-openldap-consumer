FROM jamgocoop/openldap:1.0.0
MAINTAINER Martin Espinach <martin.espinach@jamgo.coop>

ADD custom.d /srv/custom.d
COPY environment.sh /srv/

CMD ["slapd", "-h", "ldapi:/// ldap:///", "-u", "ldap", "-g", "ldap", "-d", "none"]
