# OpenLDAP consumer

Draft. Testing.

```
docker run --name ldap \
  -v /etc/localtime:/etc/localtime:ro \
  -v /etc/timezone:/etc/timezone:ro \
  -e LDAP_DOMAIN=<domain> \
  -e LDAP_DOMAIN_CREATE="false" \
  -e LDAP_PASSWORD=<admin password> \
  -e LDAP_LOG_LEVEL="none" \
  -e LDAP_REPLICATOR_USER=<replicator user> \
  -e LDAP_REPLICATOR_PASSWORD=<replicator password> \
  -e LDAP_PROVIDER_URL=<provider url> \
  -d jamgocoop/openldap-consumer:1.0.0
```